<?php
/**
 * @file
 * uw_ct_archive_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_archive_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_archive_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_ct_archive_page_user_default_permissions_alter(&$data) {
  if (isset($data['search content'])) {
    $data['search content']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['search content']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
  if (isset($data['use advanced search'])) {
    $data['use advanced search']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['use advanced search']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_archive_page_node_info() {
  $items = array(
    'archive_page' => array(
      'name' => t('Archive Page'),
      'base' => 'node_content',
      'description' => t('Archive pages only have a main area and a Subject Area taxonomy.'),
      'has_title' => '1',
      'title_label' => t('Heading'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
