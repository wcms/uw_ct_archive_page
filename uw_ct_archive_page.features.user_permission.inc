<?php
/**
 * @file
 * uw_ct_archive_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_archive_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create archive_page content'.
  $permissions['create archive_page content'] = array(
    'name' => 'create archive_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any archive_page content'.
  $permissions['delete any archive_page content'] = array(
    'name' => 'delete any archive_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own archive_page content'.
  $permissions['delete own archive_page content'] = array(
    'name' => 'delete own archive_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any archive_page content'.
  $permissions['edit any archive_page content'] = array(
    'name' => 'edit any archive_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own archive_page content'.
  $permissions['edit own archive_page content'] = array(
    'name' => 'edit own archive_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter archive_page revision log entry'.
  $permissions['enter archive_page revision log entry'] = array(
    'name' => 'enter archive_page revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override archive_page authored by option'.
  $permissions['override archive_page authored by option'] = array(
    'name' => 'override archive_page authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override archive_page authored on option'.
  $permissions['override archive_page authored on option'] = array(
    'name' => 'override archive_page authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override archive_page promote to front page option'.
  $permissions['override archive_page promote to front page option'] = array(
    'name' => 'override archive_page promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override archive_page published option'.
  $permissions['override archive_page published option'] = array(
    'name' => 'override archive_page published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override archive_page revision option'.
  $permissions['override archive_page revision option'] = array(
    'name' => 'override archive_page revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override archive_page sticky option'.
  $permissions['override archive_page sticky option'] = array(
    'name' => 'override archive_page sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search archive_page content'.
  $permissions['search archive_page content'] = array(
    'name' => 'search archive_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
